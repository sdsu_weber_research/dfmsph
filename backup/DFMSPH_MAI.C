#include "dfmsph_def.h"

int main()
{
	code_end=10;

	INP();

	if(code_end<0)goto END;
	Elab=ECM*APT/AT;
	for(is=1;is<ikGup;is++){rhoMh[is]=-200;rhoPh[is]=-200;}
	if(key_exc==1)
	{
		HYEX();
		if(key_UC>0) HCYEX();
	}
	Uex=0.;
	UCex=0.;

	f_ex1=fopen(output1,"wt");
	if(key_exc> 0)fprintf(f_ex1,"\n  RCM  iter     UCD%1ld        UCEX%1ld       UC%1ld        UND%1ld        UNEX%1ld       UN%1ld        UNCD%1ld",
		key_DD,key_DD,key_DD,key_DD,key_DD,key_DD,key_DD);
	if(key_exc==0)fprintf(f_ex1,"\n  RCM  iter     UCDdel      UCexdel      UCdel      UNDdel       UNexdel       UNdel     UNCDdel");

	BARR(RCCstart,RCCfin,RCCstep);

	if(irup>irmax)irup=irmax;
	SEAL(f_ex1);
	fclose(f_ex1);

	chi2min=100;
	chi2=0;
	if(chi2max<1.)WSP_DFP();	else printf("\n no calculations of chi2 ");

	INFOR();

	END:

	if((RBC>(RCCfin-RCCstep) && RBC<(RCCfin+RCCstep)) || RBC==RCCstart)
		printf("\n<<<<< The interval (%4.2lf, %4.2lf) doesn't contain the barrier! >>>>>",RCCstart,RCCfin);

	if(code_end<0)printf("\n<<<<<<<<<<<<<<<<<<<<<<<\n<<<<<  ABNORMAL TERMINATION  >>>>> code_end=%3ld\n\n",code_end);
	else{ printf("\n<<<<<<<<<<<<<<<  SUCCESSFUL TERMINATION  >>>>>>>>>>>>>>>\n");
	printf("\n<<<<<<<<<<<<<<<<<<<< See output files >>>>>>>>>>>>>>>>>>>> \n <out_dfmsph.out>, <out1_dfmsph.dat> and <out2_dfmsph.dat> \n");}
	DFPOUT();
	return(1);
}

/*************************** BARR *****************************************/
/******************** searching for the barrier ***************************/
double BARR(double RCMini,double RCMfin, double RCMstep)
{
	printf("\n    rat     RCM   iter     UN        UND        Uex        UNCD");
	for(RCM=RCMini,ii=0;RCM>RCMfin;RCM-=RCMstep,ii++)
	{
		Entry=-6;CheckIndex(0,ii,irmax-1);
		if(k_pot[0]==DF[0] && k_pot[1]==DF[1])
		{
			for(iter=0;iter<iter_up/1;iter++)
	   		{
		 		if(key_exc==1)
				{UNCD=DFPFIN(RCM);ratiter=fabs((Umem-UDFP)/(Umem+UDFP-alittle));}
				if(key_exc==0){UNCD=DFPDEL(RCM);ratiter=alittle;}
				if(ratiter<eps_iter && iter>0)break;
		 	}
		}
		if(k_pot[0]==WS[0] && k_pot[1]==WS[1])
		{
			UNCD=WSPOT(RCM, APT13, V0WS, r0WS, a0WS)+a_coul*ZP*ZT/RCM;
		}
		if(ii==(ii/50)*50)
	  	{
     		printf("\n%9.2e %6.2f %3ld %11.4e %9.2e %11.4e %11.4e",
     			ratiter,RCM,iter,UN,UND,Uex,UNCD);
		}
  		if(UNCD>VBC){VBC=UNCD;RBC=RCM;}
		UNRDdim[ii]=UN;irup=ii;
		RCMdim[ii]=RCM;
  		fprintf(f_ex1,"\n%6.2f %3ld %11.4e %11.4e %11.4e %11.4e %11.4e %11.4e %11.4e",RCM,iter,UCD,UCex,UC,UND,Uex,UN,UNCD);
	}

	return;
}

/***************************** WSP_DFP *********************************/
/******** approx the DFP potential near the barrier by a WSP ***********/
void WSP_DFP()
{
	printf("\nWAIT ... calculating chi2 ... VBC = %6.2f MeV",VBC);
	rWSmem=10;VWSmem=-10000;aWSmem=1;
	for(rWS=rWSmin;rWS<rWSmax;rWS+=rWSstep)
 	{
		RP0WS=rWS*AP13;RT0WS=rWS*AT13;
 		for(VWS=VWSmin;VWS<VWSmax;VWS+=VWSstep)
   		{
	  		for(aWS=aWSmin;aWS<aWSmax;aWS+=aWSstep)
			{
		   		chi2=0;i1=0;
				for(ii=0;ii<irup;ii++)
				{
					if(RCMdim[ii]>RBC-1. && RCMdim[ii]<RBC+1.)
					{
				    	UN=VWS/(1.+exp((RCMdim[ii]-rWS*AP13-rWS*AT13)/aWS));
				     	epsVN=(UNRDdim[ii]-UN)/(UNRDdim[ii]+UN);
				     	chi2+=epsVN*epsVN;i1++;
					}
			  	}
		  		chi2/=i1;
		   		if(chi2<chi2min){chi2min=chi2;rWSmem=rWS;VWSmem=VWS;aWSmem=aWS;}
			}
		}
	}
	return;
}
