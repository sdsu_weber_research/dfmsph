#include <stdio.h>
#include <math.h>
#include <ncurses.h>

#define  pi   	acos(-1.)	/* 3.141592654 */
#define  ikGup  49  		/*(ikGup-1)*2=number of knots for integration
                      		must be 11 or 21 or 49 */

/******************************** M3Y Reid forces******************************/
#define  Aex1Re		4631.38 	/* first  coefficient VEX    DDM3Y1 */
#define  Aex2Re  	(-1787.13)	/* second coefficient VEX    DDM3Y1 */
#define  Aex3Re  	(-7.8474) 	/* third  coefficient VEX    DDM3Y1 */
#define  aex1Re    	4.0     	/* first  inv  diffus VEX    DDM3Y1 */
#define  aex2Re    	2.5     	/* second inv  diffus VEX    DDM3Y1 */
#define  aex3Re    	0.7072  	/* third  inv  diffus VEX    DDM3Y1 */

#define  A40Re    	7999.		/* first  coefficient VENN */
#define  A25Re     	(-2134.)	/* second coefficient VENN */
#define  AdelRe    	(-276.)		/* delta-function  coefficient VENN-24.5 it was */
#define  CElRe    	(0.002)		/* delta-function  coefficient VENN-24.5 it was */
#define  a40Re    	4.0			/* first inverse diffusiness VENN */
#define  a25Re    	2.5			/* second inverse diffusiness VENN */

/******************************** M3Y Paris forces*****************************/
#define  Aex1Pa		(-1524.25)	/* first  coefficient VEX    DDM3Y1 */
#define  Aex2Pa  	(-518.75)	/* second coefficient VEX    DDM3Y1 */
#define  Aex3Pa  	(-7.8474) 	/* third  coefficient VEX    DDM3Y1 */
#define  aex1Pa    	4.0     	/* first  inv  diffus VEX    DDM3Y1 */
#define  aex2Pa    	2.5     	/* second inv  diffus VEX    DDM3Y1 */
#define  aex3Pa    	0.7072  	/* third  inv  diffus VEX    DDM3Y1 */

#define  A40Pa    	11061.625	/* first  coefficient VENN */
#define  A25Pa     	(-2537.5)	/* second coefficient VENN */
#define  AdelPa    	(-592.)		/* delta-function  strength Ism (6b) */
#define  CElPa    	(0.002)		/* delta-function  En dep Ism (6b) */
#define  a40Pa    	4.0			/* first inverse diffusiness VENN */
#define  a25Pa    	2.5			/* second inverse diffusiness VENN */

#define  a_coul		1.44 		/* MeV e^2*k_el */
#define  m_nucleon	0.01044		/* 1e-42*MeV*sec**2/fm**2 */
#define  hbar       0.658       /* 1e-21*MeV*sec */
#define  CS         0.027800278 /* coefficient for Fermi momentum */
#define  irmax      10000      	/* dimention of the U(R) array */
#define  output1    "out1_dfmsph.dat"   /* the first output file */
#define  output2    "out1_dfmsph.dat"   /* the second output file */
#define  input		"inp_dfmsph.inp"	/* input file */
#define  alittle    1e-10

int	NUM;

long	code_end,
	    Entry,
		idif,
		i1,ii,it,iter,
		iAP,iAT,iNUC,iZP,iZT,
		irup,ir,is,
		key_DD,key_densP,key_densT,key_exc,key_UC,key_vNN,
		priz,
		typ;

double	absGRADrho,
		alDD,a0WS,
		aWS,aWSmin,aWSmax,aWSstep,aWSmem,
		AdelCorr,
		A40,A25,Adel,a40,a25,
		Aex1,Aex2,Aex3,aex1,aex2,aex3,
		AP,AT,APT,AP13,AT13,APT13,

		beDD,
		VBC,BZ,

		chi2,chi2max,chi2min,CDD,CEl,Crup,

		delta,distP,distT,
		DxDr,D2xDr2,DrhoDr,D2rhoDr2,

		eps_iter,epsVN,
		Elab,ECM,

		gDD,

		iter_up,

		k,k_up,
		key_C,key_D,key_dP,key_dT,key_vN,key_ex,

		LAPLACErho,

		m_red,

		Nrho_centCP,Nrho_centCT,

		r,r0,r0WS,RBC,ratiter,rho,rup,
		msrp,msrn,
		rWS,rWSmin,rWSmax,rWSstep,rWSmem,
		R,RCM,
		RCCstart,RCCfin,RCCstep,
		RPav,RP0WS,RTav,RT0WS,

		sig,sup,

		tup,

		UC, UCD, UCex,
		Umem,
		Uex, 	/* nuclear exchange DF potential */
		UNCD,	/* nuclear direct+Coulomb DF potential */
		UND, 	/* nuclear direct DF potential */
		UN,UDFP,U00P,U00M,
		UC00P,UC00M,

		V0WS,
		VWS,VWSmin,VWSmax,VWSstep,VWSmem,

		ZP,ZT;

double	alDDdim[10],
		avdim[2][19],
		AGden[2][19],
		AprimeP[2][2][ikGup+1],AprimeM[2][2][ikGup+1],

		beDDdim[10],

		CDDdim[10],

		diffusP[2],diffusT[2],
		diffusPmem[3],diffusTmem[3],
		diffusCP,diffusCT,diffusDP[3],diffusDT[3],

		GCexMdim[ikGup+1],GCexPdim[ikGup+1],
		GexMdim[ikGup+1],GexPdim[ikGup+1],gDDdim[10],


		hCdim[ikGup+1][ikGup+1][2],
		hCdimMM[ikGup+1][ikGup+1][2],hCdimPM[ikGup+1][ikGup+1][2],
        hCdimMP[ikGup+1][ikGup+1][2],hCdimPP[ikGup+1][ikGup+1][2],
		hdim[ikGup+1][ikGup+1][2],
		hdimMM[ikGup+1][ikGup+1][2],hdimPM[ikGup+1][ikGup+1][2],
        hdimMP[ikGup+1][ikGup+1][2],hdimPP[ikGup+1][ikGup+1][2],
		hdirdimP[2][7][ikGup+1],hdirdimM[2][7][ikGup+1],

		IntMh[ikGup+1],IntPh[ikGup+1],

		j[9][4001],

		kFBdim[100][19],key_difP[4],key_difT[4],
		kMdim[ikGup+1],kPdim[ikGup+1],

		Nrho_centDP[2],Nrho_centDT[2],

		QGdendim[100][19],

		RCMdim[irmax],
		RGdendim[100][19],RGden[2][19],
		R0[2],RP0[3],RT0[3],
		rhoMh[ikGup+1],rhoPh[ikGup+1],

		sssM[ikGup+1],sssP[ikGup+1],

		tttM[ikGup+1],tttP[ikGup+1],

		UNRDdim[irmax],

		WW[ikGup+1],WW20[10+1],WW40[20+1],WW96[48+1],

		XX[ikGup+1],XX20[10+1],XX40[20+1],XX96[48+1],

        ydim  [ikGup+1][ikGup+1][2],
        ydimMM[ikGup+1][ikGup+1][2],ydimPM[ikGup+1][ikGup+1][2],
        ydimMP[ikGup+1][ikGup+1][2],ydimPP[ikGup+1][ikGup+1][2],

		zdim  [ikGup+1][ikGup+1][2][2],
		zdirdimP[2][7][ikGup+1],zdirdimM[2][7][ikGup+1],
		zdimMM[ikGup+1][ikGup+1][2][2],zdimPM[ikGup+1][ikGup+1][2][2],
        zdimMP[ikGup+1][ikGup+1][2][2],zdimPP[ikGup+1][ikGup+1][2][2];

char	DF[2],WS[2],k_pot[2],Title[110],Title1[110];

FILE *f_ex1,*f_ex2,*f_in;

/****************************<aexp_mai.c>****************************/
void BARR(double RCMini,double RCMfin, double RCMstep);
void WSP_DFP();	/* approx the DFP potential near the barrier by a WSP */

/****************************<aexp_fun.c>****************************/
long CheckIndex(long down, long index, long up);
double j0BRay(double x);
double Dj0BRayDx(double x);
double D2j0BRayDx2(double x);
void GAUSS();
int Number();

/****************************<aexp_inp.c>****************************/
void DFP_INI();
void INP();

/****************************<aexp_pot.c>****************************/
double A12NEW(double k, double a, double b);
double HDIR(double k, double a, double b);
double ZDIR(double k, double a, double b);
void   APM();
double VNNDIR(double k);
double VNNEXCDEL(double k);
double UNDIR(double a, double b);
double UNEXCDEL(double a, double b);
double UC00(double a, double b);
double DFPFIN(double rCC);
double DFPDEL(double rCC);
double WSPOT(double rCC, double A12, double V0, double r0, double a0);

/****************************aexp_exc.c******************************/
double KFERMI(double r, long iNUC);
double HEX(double t, double s, long iNUC);
double YEX(double t, double s, long iNUC);
double ZEX(double t, double s, long iNUC, long m);
void   HYEX();
void   HCYEX();
double VEX(double s);
double VCEX(double s);
double GEX(double R, double s);
double GCEX(double R, double s);
double UEX(double El, double R);
double UCEX(double El, double R);

/****************************aexp_rho.c******************************/
double RHOrHO(double r, long iN);
double RHOrSG(double r, long iN);
double RHOrWS(double r, long iNUC);
double RHOrFB(double r, long iN);
double RHOCENTDEF(double A, double R0, double a0);
void   RHOINI();
void   FOURBESS();
void   GAUSSDENS();

/****************************aexp_out.c******************************/
void DFPOUT();
void INFOR();
void SEAL(FILE *f_exn);
