#include "dfmsph_def.h"

/*******************Initialization of parameters*********************/
void DFP_INI()
{
	static long iik,i;
	for(is=0;is<ikGup;is++)for(it=0;it<ikGup;it++)for(iNUC=0;iNUC<2;iNUC++)
	{
		hdimMP[is][it][iNUC]=0;ydimMP[is][it][iNUC]=0;zdimMP[is][it][iNUC][0]=0;zdimMP[is][it][iNUC][1]=0;
		hCdimMP[is][it][iNUC]=0;
   		hdimPP[is][it][iNUC]=0;ydimPP[is][it][iNUC]=0;zdimPP[is][it][iNUC][0]=0;zdimPP[is][it][iNUC][1]=0;
		hCdimPP[is][it][iNUC]=0;
		hdimPM[is][it][iNUC]=0;ydimPM[is][it][iNUC]=0;zdimPM[is][it][iNUC][0]=0;zdimPM[is][it][iNUC][1]=0;
		hCdimPM[is][it][iNUC]=0;
		hdimMM[is][it][iNUC]=0;ydimMM[is][it][iNUC]=0;zdimMM[is][it][iNUC][0]=0;zdimMM[is][it][iNUC][1]=0;
		hCdimMM[is][it][iNUC]=0;
		hdim[is][it][iNUC]=0;ydim[is][it][iNUC]=0;zdim[is][it][iNUC][0]=0;zdim[is][it][iNUC][1]=0;
		hCdim[is][it][iNUC]=0;
	}
	for(is=0;is<ikGup;is++){IntMh[is]=0;IntPh[is]=0;}
	if(ikGup!=11 && ikGup!=21 && ikGup!=49){printf("\n<<<<<<<<< wrong ikGup=%d>>>>>>>>>>\n\n",ikGup);code_end=-50;return;}
	if(key_dP!=0 && key_dP!=2 && key_dP!=3){printf("\n<<<<<<<<<< wrong key_densP=%lf>>>>>>>>>>\n\n",key_dP);code_end=-51;return;}
	if(key_dT!=0 && key_dT!=2){printf("\n<<<<<<<<< wrong key_densT=%lf>>>>>>>>>>\n\n",key_dT);code_end=-51;return;}
	key_densP=key_dP/1;key_densT=key_dT/1;
	for(iik=1;iik<ikGup;iik++)
  	{
		if(ikGup==11){XX[iik]=XX20[iik];WW[iik]=WW20[iik];}
	 	if(ikGup==21){XX[iik]=XX40[iik];WW[iik]=WW40[iik];}
	 	if(ikGup==49){XX[iik]=XX96[iik];WW[iik]=WW96[iik];}
	 	kPdim[iik]=( XX[iik]*(k_up-0.)+k_up+0.)/2.;
	 	kMdim[iik]=(-XX[iik]*(k_up-0.)+k_up+0.)/2.;
	}
	VBC=-100;
	RPav=RTav=-100;
	code_end=10;

	iZP=ZP;
	iAP=AP;
	iZT=ZT;
	iAT=AT;
	m_red=m_nucleon*AP*AT/(AP+AT);
	APT=AP+AT;
	AP13=pow(AP,0.3333333);AT13=pow(AT,0.3333333);APT13=AP13+AT13;

	distP=(msrp-msrn*(AP-ZP)/ZP)*5./pi/pi/7.;
	distT=(msrp-msrn*(AT-ZT)/ZT)*5./pi/pi/7.;


	for(i=0;i<4;i++){key_difP[i]=0;key_difT[i]=0;}
	for(i=0;i<2;i++)
    {
		if(diffusPmem[i]>0. && RP0[i]>0.) {diffusDP[i]=diffusPmem[i];key_difP[i]=1;} else key_difP[i]=-1;
		key_difP[3]+=key_difP[i];
		if(diffusTmem[i]>0. && RT0[i]>0.) {diffusDT[i]=diffusTmem[i];key_difT[i]=1;} else key_difT[i]=-1;
		key_difT[3]+=key_difT[i];
	}
	if(diffusPmem[2]>0. && RP0[2]>0.) {diffusCP=diffusPmem[2];key_difP[2]=1;} else key_difP[2]=-1;
	key_difP[3]+=key_difP[2];
	if(key_difP[3]==-3.)
	{
		diffusDP[0]=diffusDP[1]=diffusCP=0.5;
		RP0[0]=RP0[1]=RP0[2]=1.1*AP13;
	}
	if(key_difP[3]!=3. && key_difP[3]!=-3.)
	{
		if(key_difP[1]<0.)
		{
			if(key_difP[0]<0.)
			{
				diffusDP[0]=sqrt(diffusCP*diffusCP-distP);
				RP0[0]=RP0[2];
			}
			else
			{
				if(key_difP[2]<0.)
				{
					diffusCP=sqrt(diffusDP[0]*diffusDP[0]+distP);
					RP0[2]=RP0[0];
				}
			}
			diffusDP[1]=diffusDP[0];RP0[1]=RP0[2];
		}
		else
		{
			if(key_difP[0]<0.)
			{
				if(key_difP[2]>0.)
				{
					diffusDP[0]=sqrt(diffusCP*diffusCP-distP);
				}
				else
				{
					diffusDP[0]=diffusDP[1];
					diffusCP=sqrt(diffusDP[0]*diffusDP[0]+distP);
					RP0[2]=RP0[1];
				}
				RP0[0]=RP0[2];
			}
			else
			{
				diffusCP=sqrt(diffusDP[0]*diffusDP[0]+distP);
				RP0[2]=RP0[0];
			}
		}
	}

	if(diffusTmem[2]>0. && RT0[2]>0.) {diffusCT=diffusTmem[2];key_difT[2]=1;} else key_difT[2]=-1; key_difT[3]+=key_difT[2];
	if((key_difT[3])==-3.)
	{
		diffusDT[0]=diffusDT[1]=diffusCT=0.5;
		RT0[0]=RT0[1]=RT0[2]=1.1*AT13;
	}
	if(key_difT[3]!=3 && key_difT[3]!=-3)
	{
		if(key_difT[1]<0)
		{
			if(key_difT[0]<0)
			{
				diffusDT[0]=sqrt(diffusCT*diffusCT-distT);
				RT0[0]=RT0[2];
			}
			else
			{
				if(key_difT[2]<0)
				{
					diffusCT=sqrt(diffusDT[0]*diffusDT[0]+distT);
					RT0[2]=RT0[0];
				}
			}
			diffusDT[1]=diffusDT[0];RT0[1]=RT0[2];
		}
		else
		{
			if(key_difT[0]<0)
			{
				if(key_difT[2]>0)
				{
					diffusDT[0]=sqrt(diffusCT*diffusCT-distT);
				}
				else
				{
					diffusDT[0]=diffusDT[1];
					diffusCT=sqrt(diffusDT[0]*diffusDT[0]+distT);
					RT0[2]=RT0[1];
				}
				RT0[0]=RT0[2];
			}
			else
			{
				diffusCT=sqrt(diffusDT[0]*diffusDT[0]+distT);
				RT0[2]=RT0[0];
			}
		}
	}

	BZ=ZP*ZT/(AP13+AT13);
	if(ECM<=0)ECM=BZ;
	chi2min=-100;
	RHOINI();
	r0=-10;

	key_DD=key_D/1;
	if(key_DD<9 && key_DD>-1){CDD=CDDdim[key_DD];alDD=alDDdim[key_DD];beDD=beDDdim[key_DD];gDD=gDDdim[key_DD];}
	else {printf("\n<<<<<<<<< Wrong key_DD=%ld. It must be integer from 0 to 8 >>>>>>>>>>\n\n",key_DD);code_end=-52;return;}

	if(key_vNN==0)
	{
		Aex1=Aex1Re;
		Aex2=Aex2Re;
		Aex3=Aex3Re;
		aex1=aex1Re;
		aex2=aex2Re;
		aex3=aex3Re;
		A40 =A40Re;
		A25 =A25Re;
		Adel=AdelRe+AdelCorr;
		CEl =CElRe;
		a40 =a40Re;
		a25 =a25Re;
	}
	if(key_vNN==1)
	{
		Aex1=Aex1Pa;
		Aex2=Aex2Pa;
		Aex3=Aex3Pa;
		aex1=aex1Pa;
		aex2=aex2Pa;
		aex3=aex3Pa;
		A40 =A40Pa;
		A25 =A25Pa;
		Adel=AdelPa+AdelCorr;
		CEl =CElPa;
		a40 =a40Pa;
		a25 =a25Pa;
	}

	iNUC=1;
	APM();
   	return;
}

/**********************<Function INPUT>*****************************/
void INP()
{
	
	char* gresult = NULL;
	int fresult = 0;

	if( (f_in=fopen(input,"r"))==NULL){
		printf("== unable to open file inp_dfmsph.inp=====\n");
		code_end=-1;
		return;
	}
	else
		printf("\n=== file inp_dfmsph.inp is opened successfully ==== \n");
	
	gresult = fgets(Title,sizeof(Title),f_in);
	if (gresult == NULL) {
		printf("== error on fgets ==\n");
		code_end=-1;
		return;
	}

	fresult = fscanf(f_in,"%c%c\n",&k_pot[0],&k_pot[1]);
	if (fresult == 0) {
		printf("== error on fscanf ==\n");
		code_end=-1;
		return;
	}
	
	gresult = fgets(Title1,sizeof(Title1),f_in);
	if (gresult == NULL) {
		printf("== error on fgets ==\n");
		code_end=-1;
		return;
	}
	
	fresult = fscanf(f_in,"%lf\n",&ECM);
	if (fresult == 0) {
		printf("== error on fscanf ==\n");
		code_end=-1;
		return;
	}
	
	gresult = fgets(Title1,sizeof(Title1),f_in);
	if (gresult == NULL) {
		printf("== error on fgets ==\n");
		code_end=-1;
		return;
	}
	
	fresult = fscanf(f_in,"%lf%lf%lf%lf%lf\n",&ZP,&AP,&RP0[0],&diffusPmem[0],&key_dP);
	if (fresult == 0) {
		printf("== error on fscanf ==\n");
		code_end=-1;
		return;
	}
	
	gresult = fgets(Title1,sizeof(Title1),f_in);
	if (gresult == NULL) {
		printf("== error on fgets ==\n");
		code_end=-1;
		return;
	}
	
	fresult = fscanf(f_in,"%lf%lf\n",&RP0[1],&diffusPmem[1]);
	if (fresult == 0) {
		printf("== error on fscanf ==\n");
		code_end=-1;
		return;
	}
	
	gresult = fgets(Title1,sizeof(Title1),f_in);
	if (gresult == NULL) {
		printf("== error on fgets ==\n");
		code_end=-1;
		return;
	}
	
	fresult = fscanf(f_in,"%lf%lf\n",&RP0[2],&diffusPmem[2]);
	if (fresult == 0) {
		printf("== error on fscanf ==\n");
		code_end=-1;
		return;
	}
	
	gresult = fgets(Title1,sizeof(Title1),f_in);
	if (gresult == NULL) {
		printf("== error on fgets ==\n");
		code_end=-1;
		return;
	}
	
	fresult = fscanf(f_in,"%lf%lf%lf%lf%lf\n",&ZT,&AT,&RT0[0],&diffusTmem[0],&key_dT);
	if (fresult == 0) {
		printf("== error on fscanf ==\n");
		code_end=-1;
		return;
	}
	
	gresult = fgets(Title1,sizeof(Title1),f_in);
	if (gresult == NULL) {
		printf("== error on fgets ==\n");
		code_end=-1;
		return;
	}
	
	fresult = fscanf(f_in,"%lf%lf\n",&RT0[1],&diffusTmem[1]);
	if (fresult == 0) {
		printf("== error on fscanf ==\n");
		code_end=-1;
		return;
	}
	
	gresult = fgets(Title1,sizeof(Title1),f_in);
	if (gresult == NULL) {
		printf("== error on fgets ==\n");
		code_end=-1;
		return;
	}
	
	fresult = fscanf(f_in,"%lf%lf\n",&RT0[2],&diffusTmem[2]);
	if (fresult == 0) {
		printf("== error on fscanf ==\n");
		code_end=-1;
		return;
	}
	
	gresult = fgets(Title1,sizeof(Title1),f_in);
	if (gresult == NULL) {
		printf("== error on fgets ==\n");
		code_end=-1;
		return;
	}
	
	fresult = fscanf(f_in,"%lf%lf\n",&msrp,&msrn);
	if (fresult == 0) {
		printf("== error on fscanf ==\n");
		code_end=-1;
		return;
	}
	
	gresult = fgets(Title1,sizeof(Title1),f_in);
	if (gresult == NULL) {
		printf("== error on fgets ==\n");
		code_end=-1;
		return;
	}
	
	fresult = fscanf(f_in,"%lf%lf%lf%lf%lf%lf\n",&iter_up,&key_ex,&key_vN,&key_D,&key_C,&AdelCorr);
	if (fresult == 0) {
		printf("== error on fscanf ==\n");
		code_end=-1;
		return;
	}
	
	gresult = fgets(Title1,sizeof(Title1),f_in);
	if (gresult == NULL) {
		printf("== error on fgets ==\n");
		code_end=-1;
		return;
	}
	
	fresult = fscanf(f_in,"%lf%lf%lf%lf%lf%lf%lf\n",&k_up,&Crup,&eps_iter,&chi2max,&RCCstart,&RCCfin,&RCCstep);
	if (fresult == 0) {
		printf("== error on fscanf ==\n");
		code_end=-1;
		return;
	}
	
	gresult = fgets(Title1,sizeof(Title1),f_in);
	if (gresult == NULL) {
		printf("== error on fgets ==\n");
		code_end=-1;
		return;
	}
	
	fresult = fscanf(f_in,"%lf%lf%lf%lf%lf%lf\n",&rWSmin,&rWSmax,&rWSstep,&VWSmin,&VWSmax,&VWSstep);
	if (fresult == 0) {
		printf("== error on fscanf ==\n");
		code_end=-1;
		return;
	}
	
	gresult = fgets(Title1,sizeof(Title1),f_in);
	if (gresult == NULL) {
		printf("== error on fgets ==\n");
		code_end=-1;
		return;
	}
	
	fresult = fscanf(f_in,"%lf%lf%lf\n",&aWSmin,&aWSmax,&aWSstep);
	if (fresult == 0) {
		printf("== error on fscanf ==\n");
		code_end=-1;
		return;
	}
	
	gresult = fgets(Title1,sizeof(Title1),f_in);
	if (gresult == NULL) {
		printf("== error on fgets ==\n");
		code_end=-1;
		return;
	}
	
	for(is=0;is<9;is++) {
		fresult = fscanf(f_in,"%lf%lf%lf%lf %c%c%c%c%c%c\n",
			&CDDdim[is],&alDDdim[is],&beDDdim[is],&gDDdim[is],
			&Title1[1],&Title1[2],&Title1[3],&Title1[4],&Title1[5],&Title1[6]);
		if (fresult == 0) {
			printf("== error on fscanf ==\n");
			code_end=-1;
			return;
		}
	}

	gresult = fgets(Title1,sizeof(Title1),f_in);
	if (gresult == NULL) {
		printf("== error on fgets ==\n");
		code_end=-1;
		return;
	}
	
	fresult = fscanf(f_in,"%lf%lf%lf\n",
		 		&V0WS,&r0WS,&a0WS);
	if (fresult == 0) {
		printf("== error on fscanf ==\n");
		code_end=-1;
		return;
	}

	if(k_pot[0]==WS[0] && k_pot[1]==WS[1]) 
		printf("\n%lf %lf %lf\n",V0WS,r0WS,a0WS);
	
	gresult = fgets(Title1,sizeof(Title1),f_in);
	if (gresult == NULL) {
		printf("== error on fgets ==\n");
		code_end=-1;
		return;
	}
	
	fclose(f_in);
	printf("\n=== file inp_dfmsph.inp is closed successfully == \n");
	
	if(ZT>AT){printf("\n ZT=%4.0f > AT=%4.0f===== \n",ZT,AT);code_end=-10;return;}
	if(ZP>AP){printf("\n ZP=%4.0f > AP=%4.0f===== \n",ZP,AP);code_end=-10;return;}
	if(ZP<=1 || ZT<=1 || AP<=1 || AT<=1){printf("\n ==== Mass and charge numbers should be more than 1 ==== \n");code_end=-10;return;}
	if(key_ex!=0 && key_ex!=1){printf("\n ======= Bad parameter key_exc=%lf. It must be 0 or 1 ==== \n",key_ex);code_end=-110;return;}
	key_exc=key_ex/1;
	if(key_exc==0){key_DD=0;key_D=0.;}
	if(key_vN!=0 && key_vN!=1){printf("\n ======= Bad parameter key_vNN=%lf. It must be 0 or 1 ==== \n",key_vN);code_end=-115;return;}
	key_vNN=key_vN/1;
	if(key_C!=0 && key_C!=1){printf("\n ======= Bad parameter key_UC=%lf. It must be 0 or 1 ==== \n",key_C);code_end=-116;return;}
	key_UC=key_C/1;

	GAUSS();
	FOURBESS();
	GAUSSDENS();
	DFP_INI();
	NUM = Number();

  	printf("\n =====DOUBLE Folding nucleus-nucleus potential===== \n");
  	if(key_densP==0)
	{
		printf("\n ..ZP ..AP ...RP0p  diffusPp  ...RP0n  diffusPn ...RP0ch  diffusPch ");
  		printf("\n %4.0f %4.0f %7.4f  %7.4f   %7.4f  %7.4f   %7.4f  %7.4f ",
  					ZP,AP,RP0[0],diffusP[0],RP0[1],diffusP[1],RP0[2],diffusCP);
	}
	else
	{
		printf("\n ..ZP ..AP ");
  		printf("\n %4.0f %4.0f ",ZP,AP);
	}
	if(key_densT==0)
	{
		printf("\n ..ZT ..AT ...RT0p  diffusTp  ...RT0n  diffusTn ...RT0ch  diffusTch ");
  		printf("\n %4.0f %4.0f %7.4f  %7.4f   %7.4f  %7.4f   %7.4f  %7.4f ",
  					ZT,AT,RT0[0],diffusT[0],RT0[1],diffusT[1],RT0[2],diffusCT);
	}
	else
	{
		printf("\n ..ZT ..AT ");
  		printf("\n %4.0f %4.0f ",ZT,AT);
	}

	printf("\n .......WAIT, PLEASE........");
  	WS[0]='W';WS[1]='S';DF[0]='F';DF[1]='P';
	if(k_pot[0]!=WS[0] || k_pot[1]!=WS[1])
	{
		if(k_pot[0]!=DF[0] || k_pot[1]!=DF[1])
		{printf("\n unknown potential");code_end=-7;}
	}
	return;
}
