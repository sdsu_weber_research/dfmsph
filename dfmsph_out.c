#include "dfmsph_def.h"

/********************void DFPOUT()*******************************************/
void DFPOUT()
{
	FILE *Out;
	Out=fopen("out_dfmsph.out","a");

	fprintf(Out,"%4d ",NUM);
	for(i1=4;i1<90;i1++)fprintf(Out,"%c",Title[i1]);
	if(k_pot[0]==WS[0])
	{
		fprintf(Out,"  Woods-Saxon   ");
		fprintf(Out,"k_pot, V0WS, r0WS, a0WS, RBC, VBC: ");
	  	fprintf(Out," %c%c, %9.2e,%9.2e,%9.2e, %5.2lf, %6.2lf\n",
	          k_pot[0],k_pot[1],V0WS,r0WS,a0WS,RBC, VBC);
	}

	if(k_pot[0]==DF[0])
	{
	  	fprintf(Out," Double-Folding");
		fprintf(Out," %3.0f %3.0f %6.3f %6.3f %6.3f %6.3f %6.3f %6.3f %3ld ",
				    	ZP,AP,RP0[0],diffusDP[0],RP0[1],diffusDP[1],RP0[2],diffusCP,key_densP);
		fprintf(Out," %3.0f %3.0f %6.3f %6.3f %6.3f %6.3f %6.3f %6.3f %3ld  %5.4f %5.4f %5.2f %5.2f  %2d",
						ZT,AT,RT0[0],diffusDT[0],RT0[1],diffusDT[1],RT0[2],diffusCT,key_densT,msrp,msrn,Crup,k_up,ikGup);
		fprintf(Out,"   %2ld    %2ld   %2ld   %2ld",key_exc,key_vNN,key_DD,key_UC);
		fprintf(Out,"   %c%c  ",k_pot[0],k_pot[1]);
		fprintf(Out,"%6.4f %6.4f %6.4f %6.4f %7.0f    ",
					CDDdim[key_DD],alDDdim[key_DD],beDDdim[key_DD],gDDdim[key_DD],AdelCorr);
		if(key_exc==0 && key_vNN==0)fprintf(Out,"Rdel000");
		if(key_exc==0 && key_vNN==1)fprintf(Out,"Pdel000");
		if(key_exc==1 && key_vNN==0)fprintf(Out,"RfinDD%1ld",key_DD);
		if(key_exc==1 && key_vNN==1)fprintf(Out,"PfinDD%1ld",key_DD);
		if(key_densP==0 || key_densP==-1)fprintf(Out," PWS");
		if(key_densP==1)fprintf(Out," PFB");
		if(key_densP==2)fprintf(Out," PSG");
		if(key_densP==3)fprintf(Out," PHO");
		if(key_densT==0 || key_densT==-1)fprintf(Out," TWS");
		if(key_densT==1)fprintf(Out," TFB");
		if(key_densT==2)fprintf(Out," TSG");
		fprintf(Out," %6.2f %6.2f %7.3f %7.1f %5.2f %6.3f",
			   		  ECM,RBC,VBC,VWSmem,rWSmem,aWSmem);
		fprintf(Out,"  %8.1e \n",chi2min);
	}

	fclose(Out);
	return;
}

/*******************INFOR*************************************************************/
void INFOR()
{
   	f_ex2=fopen(output2,"wt");
   	if(k_pot[0]==DF[0] && k_pot[1]==DF[1])fprintf(f_ex2,"\n  RCM    UNDFP    UNWSPfit");
   	if(k_pot[0]==WS[0] && k_pot[1]==WS[1])fprintf(f_ex2,"\n  RCM    UNWS0    UNWSPfit");
   	for(ii=0;ii<irup;ii++)fprintf(f_ex2,"\n%6.2f %11.4e %11.4e ",
   	RCMdim[ii],UNRDdim[ii],WSPOT(RCMdim[ii],AP13+AT13,VWSmem,rWSmem,aWSmem));
   	fprintf(f_ex2,"\n  RCM=%7.2f ... %7.2f;  chi2max=%8.3f",
   	RCMdim[0],RCMdim[irup-1],chi2max);
   	fprintf(f_ex2,"\n  rWS        VWS  aWS   chi2%1ld",key_DD);
   	fprintf(f_ex2,"\n%6.3f %11.4e %6.3f %11.4e ",rWSmem,VWSmem,aWSmem,chi2min);
	SEAL(f_ex2);
   	fclose(f_ex2);
   	return;
}

/*********** SEAL SEAL **************************************************/
void SEAL(FILE *f_exn)
{
	static long i1;
	fprintf(f_exn,"\n");
	fprintf(f_exn,"%4d ",NUM);
	for(i1=0;i1<80;i1++)fprintf(f_exn,"%c",Title[i1]);
	fprintf(f_exn,"\n<<<<<<<<<<<<<<< RBC=%6.3f VBC=%6.3f >>>>>>>>>>>>>>>",RBC,VBC);

	if(k_pot[0]==WS[0])
	{
		fprintf(f_exn,"\n Woods-Saxon");
		fprintf(f_exn,"\nk_pot, V0WS,    r0WS,   a0WS ");
	  	fprintf(f_exn,"\n  %c%c,  %9.2e %6.3e %6.3e",
	          		k_pot[0],k_pot[1],V0WS,r0WS,a0WS);
	}

	if(k_pot[0]==DF[0])
	{
	  	fprintf(f_exn,"\n Double-Folding");
	  	fprintf(f_exn,"\n  ZP  AP    RP0   difDPp  difDPn  difCP  keydP  Crup  k_up");
	  	fprintf(f_exn,"\n %3.0f %3.0f %6.3f %6.3f  %6.3f %6.3f %3ld  %6.2f %6.2f",
	  					ZP,AP,RP0[0],diffusDP[0],diffusDP[1],diffusCP,key_densP,Crup,k_up);
	  	fprintf(f_exn,"\n  ZT  AT    RT0   difDTp  difDTn  difCT  keydT  k_up ikGup");
	  	fprintf(f_exn,"\n %3.0f %3.0f %6.3f %6.3f  %6.3f  %6.3f %3ld  %6.2f %4d",
	  					ZT, AT, RT0[0], diffusDT[0],diffusDT[1],diffusCT,key_densT,k_up,ikGup);
	  	fprintf(f_exn,"\n ...rup ...sup ...tup   ECM  key_exc key_vNN key_DD key_UC");
	  	fprintf(f_exn,"\n %6.3f %6.3f %6.3f %6.2f    %2ld     %2ld      %2ld     %2ld",
	  					rup,sup,tup,ECM,key_exc,key_vNN,key_DD,key_UC);
		fprintf(f_exn,"\nk_pot=  %c%c,",k_pot[0],k_pot[1]);
		if(key_densP==0 || key_densP==-1)fprintf(f_exn,"\nProj  - WS density profiles RPav=%11.4e ",RPav);
		if(key_densT==0 || key_densT==-1)fprintf(f_exn,"\nTarget- WS density profiles RTav=%11.4e ",RTav);
		if(key_densP==1)fprintf(f_exn,"\nProj  - FB density profiles RPav=%11.4e ",RPav);
		if(key_densT==1)fprintf(f_exn,"\nTarget- FB density profiles RTav=%11.4e ",RTav);
		if(key_densP==2)fprintf(f_exn,"\nProj  - SG density profiles RPav=%11.4e ",RPav);
		if(key_densT==2)fprintf(f_exn,"\nTarget- SG density profiles RTav=%11.4e ",RTav);
		if(key_densP==3)fprintf(f_exn,"\nProj  - HO density profiles RPav=%11.4e ",RPav);
		if(key_vNN==0)fprintf(f_exn,"\nReid NN-forces");
		if(key_vNN==1)fprintf(f_exn,"\nParis NN-forces");
		if(key_exc==0)fprintf(f_exn,"; delta exchange forces");
		if(key_exc==1)fprintf(f_exn,"; finite range exchange forces, eps_iter=%9.2e",eps_iter);
		if(key_DD ==0)fprintf(f_exn,";\nno density dependence in exchange forces");
		if(key_DD > 0)
		{
			fprintf(f_exn,";\ndensity dependent exchange forces:\nCDD    alDD   beDD   gDD");
			fprintf(f_exn,"\n%6.4f %6.4f %6.4f %6.4f",CDDdim[key_DD],alDDdim[key_DD],beDDdim[key_DD],gDDdim[key_DD]);
		}
		if(key_exc==0)fprintf(f_exn,"\n       del000");
		if(key_exc==1)fprintf(f_exn,"\n       finDD%ld",key_DD);
		if(key_densP==0 || key_densP==-1)fprintf(f_exn,"PWS");
		if(key_densP==1)fprintf(f_exn,"PFB");
		if(key_densP==2)fprintf(f_exn,"PSG");
		if(key_densT==0 || key_densT==-1)fprintf(f_exn,"TWS");
		if(key_densT==1)fprintf(f_exn,"TFB");
		if(key_densT==2)fprintf(f_exn,"TSG");
		fprintf(f_exn,"\nECM  %6.2f\nRBC  %6.2f\nVBC   %7.3f\nVWSmem   %6.1f\nrWSmem  %6.2f\naWSmem  %7.3f",
						ECM,RBC,VBC,VWSmem,rWSmem,aWSmem);
		fprintf(f_exn,"\nchi2   %8.1e",chi2min);
	}
	return;
}
