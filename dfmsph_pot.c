#include "dfmsph_def.h"

/*****Preparation of Aprime-coefficients by Gaussian integration*****/
double A12NEW(double k, double a, double b)
{
	static long ir,key_dens;
	static double argP,argM,integ,rhoP,rhoM;
	integ=0.;
	if(iNUC!=0 && iNUC!=1){printf("\n <<<<<< Unable to calculate APRIME: iNUC=%4ld >>>>>>>>>>>\n",iNUC);code_end=-20;getch();return(-100);}
	for(ir=1;ir<ikGup;ir++)
	{
		argP=( XX[ir]*(b-a)+b+a)/2.;
		argM=(-XX[ir]*(b-a)+b+a)/2.;
		key_dens=key_densP*(-iNUC+1)+key_densT*iNUC;
		if(key_dens==0 || key_dens==-1){rhoP=RHOrWS(argP,iNUC);rhoM=RHOrWS(argM,iNUC);}
		if(key_dens==1){rhoP=RHOrFB(argP,iNUC);rhoM=RHOrFB(argM,iNUC);}
		if(key_dens==2){rhoP=RHOrSG(argP,iNUC);rhoM=RHOrSG(argM,iNUC);}
		if(key_dens==3){rhoP=RHOrHO(argP,iNUC);rhoM=RHOrHO(argM,iNUC);}
		integ+=WW[ir]*(argP*argP*rhoP*j0BRay(argP*k)+argM*argM*rhoM*j0BRay(argM*k));
   }
	integ*=(b-a)/2.*sqrt(4.*pi);
	return(integ);
}

/******Preparation of Hdir-coefficients by Gaussian integration******/
double HDIR(double k, double a, double b)
{
	static long ir,key_dens;
	static double argP,argM,integ,rhoP,rhoM;
	integ=0.;
	if(iNUC!=0 && iNUC!=1){printf("\n <<<<<< unable to calculate HDIR: iNUC=%4ld >>>>>>>>>>>\n",iNUC);code_end=-20;return(-100);}
	for(ir=1;ir<ikGup;ir++)
	{
		argP=( XX[ir]*(b-a)+b+a)/2.;
		argM=(-XX[ir]*(b-a)+b+a)/2.;
		key_dens=key_densP*(-iNUC+1)+key_densT*iNUC;
		if(key_dens==0 || key_dens==-1){rhoP=RHOrWS(argP,iNUC);rhoM=RHOrWS(argM,iNUC);}
		if(key_dens==1){rhoP=RHOrFB(argP,iNUC);rhoM=RHOrFB(argM,iNUC);}
		if(key_dens==2){rhoP=RHOrSG(argP,iNUC);rhoM=RHOrSG(argM,iNUC);}
		if(key_dens==3){rhoP=RHOrHO(argP,iNUC);rhoM=RHOrHO(argM,iNUC);}
		integ+=WW[ir]*(argP*argP*rhoP*j0BRay(argP*k)*exp(-beDD*rhoP)
		              +argM*argM*rhoM*j0BRay(argM*k)*exp(-beDD*rhoM));
   	}
	integ*=(b-a)/2.*sqrt(4.*pi);
	return(integ);
}

/******Preparation of Zdir-coefficients by Gaussian integration******/
double ZDIR(double k, double a, double b)
{
	static long ir,key_dens;
	static double argP,argM,integ,rhoP,rhoM;

	integ=0.;
	if(iNUC!=0 && iNUC!=1){printf("\n <<<<<< unable to calculate ZDIR: iNUC=%4ld >>>>>>>>>>>\n",iNUC);code_end=-20;return(-100);}
	for(ir=1;ir<ikGup;ir++)
	{
		argP=( XX[ir]*(b-a)+b+a)/2.;
		argM=(-XX[ir]*(b-a)+b+a)/2.;
		key_dens=key_densP*(-iNUC+1)+key_densT*iNUC;
		if(key_dens==0 || key_dens==-1){rhoP=RHOrWS(argP,iNUC);rhoM=RHOrWS(argM,iNUC);}
		if(key_dens==1){rhoP=RHOrFB(argP,iNUC);rhoM=RHOrFB(argM,iNUC);}
		if(key_dens==2){rhoP=RHOrSG(argP,iNUC);rhoM=RHOrSG(argM,iNUC);}
		if(key_dens==3){rhoP=RHOrHO(argP,iNUC);rhoM=RHOrHO(argM,iNUC);}
		integ+=WW[ir]*(argP*argP*rhoP*j0BRay(argP*k)*rhoP
		              +argM*argM*rhoM*j0BRay(argM*k)*rhoM);
   }
	integ*=(b-a)/2.*sqrt(4.*pi);
	return(integ);
}

/*******************************************************************/
void APM()
{
	static long key_dens,ikG;
	static double R0[2],rdo,rup;
	for(iNUC=0;iNUC<2;iNUC++)
	{
		R0[1]=RP0[1]*(-iNUC+1)+RT0[1]*iNUC;
   		key_dens=key_densP*(-iNUC+1)+key_densT*iNUC;
   		rdo=0;
	 	if(key_dens==0 || key_dens==-1)rup=Crup*R0[1];
	 	if(key_dens==1)rup=avdim[iNUC][18];
	 	if(key_dens==2)rup=Crup*R0[1];
	 	if(key_dens==3)rup=Crup*R0[1];
		diffusP[0]=diffusCP; diffusT[0]=diffusCT;
		typ=0;
		for(ikG=ikGup-1;ikG>0;ikG--)
			AprimeM[iNUC][0][ikG]=A12NEW(kMdim[ikG],rdo,rup);
      	for(ikG=1;ikG<ikGup;ikG++)
			AprimeP[iNUC][0][ikG]=A12NEW(kPdim[ikG],rdo,rup);
		for(idif=0;idif<2;idif++)
			{diffusP[idif]=diffusDP[idif]; diffusT[idif]=diffusDT[idif];}
		typ=1;
		for(ikG=ikGup-1;ikG>0;ikG--)
		{
			AprimeM[iNUC][1][ikG]=A12NEW(kMdim[ikG],rdo,rup);
			if(key_DD>0)hdirdimM[iNUC][1][ikG]=HDIR(kMdim[ikG],rdo,rup);
			if(key_DD>0)zdirdimM[iNUC][1][ikG]=ZDIR(kMdim[ikG],rdo,rup);
		}
      	for(ikG=1;ikG<ikGup;ikG++)
		{
			AprimeP[iNUC][1][ikG]=A12NEW(kPdim[ikG],rdo,rup);
			if(key_DD>0)hdirdimP[iNUC][1][ikG]=HDIR(kPdim[ikG],rdo,rup);
			if(key_DD>0)zdirdimP[iNUC][1][ikG]=ZDIR(kPdim[ikG],rdo,rup);
	   	}
  	}
	return;
}

/*********************** Fourier transform ***************************
 *********** of direct effective nucleon-nucleon potential **********/
double VNNDIR(double k)
{
	double VNNdir;
	VNNdir=4.*pi*(A40/a40/(k*k+a40*a40)+A25/a25/(k*k+a25*a25));
	return(VNNdir);
}

/*********************** Fourier transform ***************************
 ****** of exchange (delta) effective nucleon-nucleon potential *****/
double VNNEXCDEL(double k)
{
	double VNNexcdel;
	VNNexcdel=Adel;
	return(VNNexcdel);
}

/****************** Nuclear direct DF potential *********************/
double UNDIR(double a, double b)
{
	static long ik;
	static double r,k,integ,jB0_r_k,UH00M,UH00P,UZ00M,UZ00P;
	integ=0;r=RCM;
	for(ik=1;ik<ikGup;ik++)
	{
		k=kPdim[ik];
		jB0_r_k=1.;if(fabs(r*k)>alittle)jB0_r_k=sin(r*k)/(r*k);
		U00P=VNNDIR(k)*k*k*AprimeP[0][1][ik]*AprimeP[1][1][ik]*jB0_r_k;
		UH00P=VNNDIR(k)*k*k*hdirdimP[0][1][ik]*hdirdimP[1][1][ik]*jB0_r_k;
		k=kMdim[ik];
		jB0_r_k=1.;if(fabs(r*k)>alittle)jB0_r_k=sin(r*k)/(r*k);
		U00M=VNNDIR(k)*k*k*AprimeM[0][1][ik]*AprimeM[1][1][ik]*jB0_r_k;
		UH00M=VNNDIR(k)*k*k*hdirdimM[0][1][ik]*hdirdimM[1][1][ik]*jB0_r_k;

		UZ00M=VNNDIR(k)*k*k*jB0_r_k*
       		(zdirdimM[0][1][ik]*AprimeM[1][1][ik]+
        	zdirdimM[1][1][ik]*AprimeM[0][1][ik]);
 		UZ00P=VNNDIR(k)*k*k*jB0_r_k*
       		(zdirdimP[0][1][ik]*AprimeP[1][1][ik]+
        	zdirdimP[1][1][ik]*AprimeP[0][1][ik]);

		U00P*=(2./pi);U00M*=(2./pi);UH00P*=(2./pi);UH00M*=(2./pi);UZ00P*=(2./pi);UZ00M*=(2./pi);
 		integ+=CDD*WW[ik]*(U00P+U00M+alDD*(UH00P+UH00M)-gDD*(UZ00P+UZ00M));
	}
	integ*=(b-a)/2.*(1.-CEl*Elab/(AP+alittle));
	return(integ);
}

/******************<<<<UNEXCDEL>>>*********************************************/
double UNEXCDEL(double a, double b)
{
	static long ik;
	static double k,integ,jB0_r_k;
	integ=0;r=RCM;
	for(ik=1;ik<ikGup;ik++)
	{
		k=kPdim[ik];
 		jB0_r_k=1.;if(fabs(r*k)>alittle)jB0_r_k=sin(r*k)/(r*k);
  		U00P=VNNEXCDEL(k)*k*k*AprimeP[0][1][ik]*AprimeP[1][1][ik]*jB0_r_k;
 		k=kMdim[ik];
 		jB0_r_k=1.;if(fabs(r*k)>alittle)jB0_r_k=sin(r*k)/(r*k);
  		U00M=VNNEXCDEL(k)*k*k*AprimeM[0][1][ik]*AprimeM[1][1][ik]*jB0_r_k;
 		U00P*=(2./pi);U00M*=(2./pi);
 		integ+=WW[ik]*(U00P+U00M);
	}
	integ*=(b-a)/2.*(1.-CEl*Elab/(AP+alittle));
	return(integ);
}

/*************Coul********* UC00******************************************/
double UC00(double a, double b)
{
	static long ikk;
	static double integ,k,jB0_r_k;
	integ=0;r=RCM;
	for(ikk=1;ikk<ikGup;ikk++)
	{
   		k=kPdim[ikk];
   		jB0_r_k=1.;if(fabs(r*k)>alittle)jB0_r_k=sin(r*k)/(r*k);
   		UC00P=jB0_r_k*AprimeP[0][0][ikk]*AprimeP[1][0][ikk];
   		k=kMdim[ikk];
   		jB0_r_k=1.;if(fabs(r*k)>alittle)jB0_r_k=sin(r*k)/(r*k);
   		UC00M=jB0_r_k*AprimeM[0][0][ikk]*AprimeM[1][0][ikk];
   		integ+=WW[ikk]*(UC00P+UC00M);
	}
	integ*=(b-a)/2.*4.*pi*a_coul*(2./pi)*ZP*ZT/AP/AT;
	return(integ);
}

/**************MAIN _DFP for final radius of excahnge forces************************/
double DFPFIN(double rCC)
{
	Umem=UDFP;
	UND=UNDIR(0.,k_up);
	UCD=UC00(0.,k_up);
	if(iter>0)UN=UND+Uex;else UN=UND;
	if(iter>0)UC=UCD+UCex;else UC=UCD;
	UDFP=UN+UC;
	Uex=UEX(Elab,rCC);
	UCex=UCEX(Elab,rCC);
	return(UDFP);
}

/**************MAIN _DFP for delta-like excahnge forces************************/
double DFPDEL(double rCC)
{
	if(fabs(rCC)<alittle)rCC=alittle;
	UND=UNDIR(0.,k_up);
	UC=UC00(0.,k_up);
	Uex=UNEXCDEL(0.,k_up);
	UN=UND+Uex;
	UDFP=UN+UC;
	return(UDFP);
}

/*******************************WSPOT**************************************/
double WSPOT(double rCC, double A12, double V0, double r0, double a0)
{
	UN=V0/(1.+exp((rCC-r0*A12)/a0));
	return(UN);
}

