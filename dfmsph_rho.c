#include "dfmsph_def.h"
/***********RHOrHO**Harmonic Oscillator Model Khoa PRC63 0340007************************************************/
double RHOrHO(double r, long iN)
{
	static double A,Z,b,Fp,Coef,rho,arg,DargDr,exparg_2;

	D2rhoDr2=0;DrhoDr=0;rho=alittle;
	Entry=-141;CheckIndex(0,iN,1);
	A=AP*(-iN+1)+AT*iN;
	Z=ZP*(-iN+1)+ZT*iN;
	if(fabs(A- 4.)<alittle && fabs(Z-2.)<alittle){b=1.2658;Fp=0;}
	if(fabs(A-12.)<alittle && fabs(Z-6.)<alittle){b=1.5840;Fp=4./3.;}
	if(fabs(A-16.)<alittle && fabs(Z-8.)<alittle){b=1.7410;Fp=2.;}
	if(fabs(A-16.)>alittle && fabs(A-12.)>alittle && fabs(A-4.)>alittle)
	{printf("\n Z=%3.0f A=%3.0f For this nucleus HO (Harmonic Oscillator) is NOT available. Command?",Z,A);getchar();return(1.);}

	arg=r/b;
	DargDr=1./b;
	Coef=4./pi/sqrt(pi)/b/b/b;
	exparg_2=exp(-arg*arg);
	rho=Coef*(1.+Fp*arg*arg)*exparg_2;
	DrhoDr=-2.*rho*arg*DargDr+2.*Coef*Fp*arg*DargDr*exparg_2;
	D2rhoDr2=-2.*arg*DargDr*DrhoDr+2.*Coef*Fp*DargDr*exparg_2*(DargDr-2.*arg*arg*DargDr);
	return(rho);
}

/***********RHOrSOG**sum of Gaussians*ADNDT 36(1987)495-536***Table V*************************************************/
double RHOrSG(double r, long iN)
{
	static long irho;
	static double gamG,xrho,argM,argP,DargMDr,DargPDr,exM,exP,D2argDr2;

	D2rhoDr2=0;DrhoDr=0;xrho=alittle;
	Entry=-14;CheckIndex(0,iN,1);
	gamG=sqrt(2./3.)*RGden[iN][18];
	for(irho=1;irho<18;irho++)
	{
		argM=(r-RGden[iN][irho])/gamG;argM*=-argM;
	 	DargMDr=-2.*(r-RGden[iN][irho])/gamG/gamG;
	 	argP=(r+RGden[iN][irho])/gamG;argP*=-argP;
	 	DargPDr=-2.*(r+RGden[iN][irho])/gamG/gamG;
	 	exM=exp(argM);exP=exp(argP);
	 	xrho+=AGden[iN][irho]*(exM+exP);
   		DrhoDr+=AGden[iN][irho]*(exM*DargMDr+exP*DargPDr);
   		D2rhoDr2+=AGden[iN][irho]*(exM*(DargMDr*DargMDr+D2argDr2)
	                         +exP*(DargPDr*DargPDr+D2argDr2) );
	}
	return(xrho);
}

/********************** WS profile***************************************/
double RHOrWS(double r, long iNUC)
{
	static double Nrho_cent[2],diffus[2],rhoi[2],expon;
	if(typ==0)Nrho_cent[0]=Nrho_centCP*(-iNUC+1)+Nrho_centCT*iNUC;
	else for(idif=0;idif<2;idif++) Nrho_cent[idif]=Nrho_centDP[idif]*(-iNUC+1)+Nrho_centDT[idif]*iNUC;

	rho=0.;DrhoDr=0.;D2rhoDr2=0.;
	for(idif=0;idif<2;idif++)
		{
			diffus[idif]=diffusP[idif]*(-iNUC+1)+diffusT[idif]*iNUC;
			if(typ==0)R0[idif]=RP0[2]*(-iNUC+1)+RT0[2]*iNUC;
			else R0[idif]=RP0[idif]*(-iNUC+1)+RT0[idif]*iNUC;
	        expon=exp((r-R0[idif])/diffus[idif]);
			rhoi[idif]=Nrho_cent[idif]/(1.+expon);
	        DrhoDr+=-rhoi[idif]*expon/(1.+expon)/diffus[idif];
			D2rhoDr2+=2.*rhoi[idif]*expon*expon/(1.+expon)/(1.+expon)/diffus[idif]/diffus[idif]
    	    	-rhoi[idif]*expon/(1.+expon)/diffus[idif]/diffus[idif];
			rho+=rhoi[idif];
			if(typ==0) break;
		}
	return(rho);
}

/*****************Fourier-Bessel**********ADNDT 36(1987)495-536*Table IV****************************************************/
double RHOrFB(double r, long iN)
{
	static long irho;
	static double xrho,xB,DxBDr;

	D2rhoDr2=0;DrhoDr=0;xrho=alittle;
	Entry=-14;CheckIndex(0,iN,1);
	if(r<avdim[iN][18]-alittle)for(irho=1;irho<18;irho++)
	{
   		xB=irho*pi*r/avdim[iN][18];
   		DxBDr=irho*pi/avdim[iN][18];
	 	xrho+=avdim[iN][irho]*j0BRay(xB);
  	    DrhoDr+=avdim[iN][irho]*Dj0BRayDx(xB)*DxBDr;
	 	D2rhoDr2+=avdim[iN][irho]*D2j0BRayDx2(xB)*DxBDr*DxBDr;
	}
	return(xrho);
}

/****central density for deformed nuclei ******RHOCENTDEF****************/
double RHOCENTDEF(double ANUC, double R0, double a0)
{
	static long ik,it;
	static double diffus,th,thup,r_up,cM,cP,r,rho_cent,integ,integM,integP;
	rho_cent=0;integ=0;
	thup=pi/2.;
	r_up=Crup*R0;
	for(it=1;it<ikGup;it++)
	{
		th=kMdim[it]*thup/k_up;integM=0;
	 	for(ik=1;ik<ikGup;ik++)
	 	{
			R=R0;
			diffus=a0;
	 		r=kMdim[ik]*r_up/k_up;cM=r*r/(1.+exp((r-R)/diffus));
	 		r=kPdim[ik]*r_up/k_up;cP=r*r/(1.+exp((r-R)/diffus));
	 		integM+=WW[ik]*(cP+cM)*sin(th);
	 	}
	 	integM*=(r_up-0.)/2.;
	 	th=kPdim[it]*thup/k_up;integP=0;
	 	for(ik=1;ik<ikGup;ik++)
	 	{
			R=R0;
			diffus=a0;
	 		r=kMdim[ik]*r_up/k_up;cM=r*r/(1.+exp((r-R)/diffus));
	 		r=kPdim[ik]*r_up/k_up;cP=r*r/(1.+exp((r-R)/diffus));
	 		integP+=WW[ik]*(cP+cM)*sin(th);
	 	}
	 	integP*=(r_up-0.)/2.;
	 	integ+=WW[it]*(integM+integP);
	}
	integ*=2.*(thup-0.)/2.;
	rho_cent=ANUC/2./pi/(integ+alittle);
	return(rho_cent);
}

/******************RHOINI**********************************************************/
void RHOINI()
{
	static long iINI;
	static double sum,gamG;
/********defining dinsity of projectile********************************************/
	if(key_densP==0 || key_densP==-1)
	{
		Nrho_centCP=RHOCENTDEF(AP, RP0[2], diffusCP);
		Nrho_centDP[0]=RHOCENTDEF(ZP, RP0[0], diffusDP[0]);
		Nrho_centDP[1]=RHOCENTDEF(AP-ZP, RP0[1], diffusDP[1]);
	}
	if(key_densP==1)
	{
		for(iINI=0;iINI<19;iINI++)avdim[0][iINI]=kFBdim[iZP][iINI];
  		RPav=avdim[0][0];
		if(avdim[0][18]<alittle)avdim[0][18]=alittle;
	}
	if(key_densP==2)
	{
		sum=0;
		for(iINI=0;iINI<19;iINI++)
	  	{
			RGden[0][iINI]=RGdendim[iZP][iINI];
			AGden[0][iINI]=QGdendim[iZP][iINI];sum+=AGden[0][iINI];
		}
		RPav=RGden[0][0];gamG=sqrt(2./3.)*RGden[0][18];
		RP0[0]=RPav;
		for(iINI=0;iINI<19;iINI++)
	   	{
		 	AGden[0][iINI]*=AP/(2.*pi*sqrt(pi)*gamG*
	                    (gamG*gamG+2.*RGden[0][iINI]*RGden[0][iINI]));
	    	printf("\n\nSOGP i=%2ld RG=%8.6f AG=%8.6f",
						iINI,RGden[0][iINI],AGden[0][iINI]);
		}
		printf("\n\nINP SGdensP sum=%6.3f iZP=%2ld command?",sum,iZP);getchar();
	}

/********defining dinsity of target********************************************/
	if(key_densT==0 || key_densT==-1)
	{
		Nrho_centCT=RHOCENTDEF(AT, RT0[2], diffusCT);
		Nrho_centDT[0]=RHOCENTDEF(ZT, RT0[0], diffusDT[0]);
		Nrho_centDT[1]=RHOCENTDEF(AT-ZT, RT0[1], diffusDT[1]);
	}
	if(key_densT==1)
	{
		for(iINI=0;iINI<19;iINI++)avdim[1][iINI]=kFBdim[iZT][iINI];
  		RTav=avdim[1][0];
		if(avdim[1][18]<alittle)avdim[1][18]=alittle;
		printf("\n\nINP FBdensT iINI   avdim\n");
		for(iINI=0;iINI<19;iINI++)printf(" %2ld %11.8f\n",iINI,avdim[1][iINI]);
	}
	if(key_densT==2)
	{
		sum=0;
		for(iINI=0;iINI<19;iINI++)
	  	{
			RGden[1][iINI]=RGdendim[iZT][iINI];
			AGden[1][iINI]=QGdendim[iZT][iINI];sum+=AGden[1][iINI];
		}
		RTav=RGden[1][0];gamG=sqrt(2./3.)*RGden[1][18];
		RT0[0]=RTav;
		for(iINI=0;iINI<19;iINI++)
	   	{
		 	AGden[1][iINI]*=AT/(2.*pi*sqrt(pi)*gamG*
	                    (gamG*gamG+2.*RGden[1][iINI]*RGden[1][iINI]));
	    	printf("\n\nSOGT i=%2ld RG=%8.6f AG=%8.6f",
						iINI,RGden[1][iINI],AGden[1][iINI]);
		}
		printf("\n\nINP SGdensT sum=%6.3f iZT=%2ld command?",sum,iZT);getchar();/**/
	}
	return;
}

/********************************************************************************/
void FOURBESS()
{
	long ikFB,i11;
	for(i11=0;i11<100;i11++)for(ikFB=0;ikFB<19;ikFB++)kFBdim[i11][ikFB]=0;
/* 16O  Fourier-Bessel coefficients from H.de Vrieset al. ADNDT36(1987)495*/
	kFBdim[ 8][ 0]= 2.737;
	kFBdim[ 8][ 1]= 0.20238e-1*16./8.;
	kFBdim[ 8][ 2]= 0.44793e-1*16./8.;
	kFBdim[ 8][ 3]= 0.33533e-1*16./8.;
	kFBdim[ 8][ 4]= 0.35030e-2*16./8.;
	kFBdim[ 8][ 5]=-0.12293e-1*16./8.;
	kFBdim[ 8][ 6]=-0.10329e-1*16./8.;
	kFBdim[ 8][ 7]=-0.34036e-2*16./8.;
	kFBdim[ 8][ 8]=-0.41627e-3*16./8.;
	kFBdim[ 8][ 9]=-0.94435e-3*16./8.;
	kFBdim[ 8][10]=-0.25771e-3*16./8.;
	kFBdim[ 8][11]= 0.23759e-3*16./8.;
	kFBdim[ 8][12]=-0.10603e-3*16./8.;
	kFBdim[ 8][13]= 0.41480e-4*16./8.;
	kFBdim[ 8][18]= 8.00000e-0;

/* 92Zr Fourier-Bessel coefficients from H.de Vries et al. ADNDT36(1987)495*/
	kFBdim[40][ 0]= 4.295;
	kFBdim[40][ 1]= 0.45939e-1*92./40.;
	kFBdim[40][ 2]= 0.60104e-1*92./40.;
	kFBdim[40][ 3]=-0.13341e-1*92./40.;
	kFBdim[40][ 4]=-0.35106e-1*92./40.;
	kFBdim[40][ 5]= 0.31760e-2*92./40.;
	kFBdim[40][ 6]= 0.13753e-1*92./40.;
	kFBdim[40][ 7]=-0.82682e-3*92./40.;
	kFBdim[40][ 8]=-0.53001e-2*92./40.;
	kFBdim[40][ 9]=-0.97579e-3*92./40.;
	kFBdim[40][10]= 0.26489e-3*92./40.;
	kFBdim[40][11]=-0.15873e-3*92./40.;
	kFBdim[40][12]= 0.69301e-4*92./40.;
	kFBdim[40][13]=-0.22278e-4*92./40.;
	kFBdim[40][14]= 0.39533e-5*92./40.;
	kFBdim[40][15]= 0.10609e-5*92./40.;
	kFBdim[40][18]= 1.00000e+1;

/*144Sm Fourier-Bessel coefficients from H.de Vries et al. ADNDT36(1987)495*/
	kFBdim[62][ 0]= 4.943;
	kFBdim[62][ 1]= 0.74734e-1*144./62.;
	kFBdim[62][ 2]= 0.26145e-1*144./62.;
	kFBdim[62][ 3]=-0.63832e-1*144./62.;
	kFBdim[62][ 4]= 0.10432e-1*144./62.;
	kFBdim[62][ 5]= 0.19183e-1*144./62.;
	kFBdim[62][ 6]=-0.12572e-1*144./62.;
	kFBdim[62][ 7]=-0.39707e-2*144./62.;
	kFBdim[62][ 8]=-0.18703e-2*144./62.;
	kFBdim[62][ 9]= 0.12602e-2*144./62.;
	kFBdim[62][10]=-0.11902e-2*144./62.;
	kFBdim[62][11]=-0.15703e-2*144./62.;
	kFBdim[62][18]= 9.25000e+0;

/*208Pb Fourier-Bessel coefficients from H.de Vries et al. ADNDT36(1987)495
 Eu78*/
	kFBdim[82][ 0]= 5.503;
	kFBdim[82][ 1]= 0.51936e-1*208./82.;
	kFBdim[82][ 2]= 0.50768e-1*208./82.;
	kFBdim[82][ 3]=-0.39646e-1*208./82.;
	kFBdim[82][ 4]=-0.28218e-1*208./82.;
	kFBdim[82][ 5]= 0.28916e-1*208./82.;
	kFBdim[82][ 6]= 0.98910e-2*208./82.;
	kFBdim[82][ 7]=-0.14388e-1*208./82.;
	kFBdim[82][ 8]=-0.98262e-3*208./82.;
	kFBdim[82][ 9]= 0.72578e-2*208./82.;
	kFBdim[82][10]= 0.82318e-3*208./82.;
	kFBdim[82][11]=-0.14823e-2*208./82.;
	kFBdim[82][12]= 0.13245e-3*208./82.;
	kFBdim[82][13]=-0.84345e-4*208./82.;
	kFBdim[82][14]= 0.48417e-4*208./82.;
	kFBdim[82][15]=-0.26562e-4*208./82.;
	kFBdim[82][16]= 0.14035e-4*208./82.;
	kFBdim[82][17]=-0.71863e-5*208./82.;
	kFBdim[82][18]= 1.20000e+1;

	return;
}

void GAUSSDENS()
{
	long ik,i11;
	for(i11=0;i11<100;i11++)for(ik=0;ik<19;ik++){QGdendim[i11][ik]=0;RGdendim[i11][ik]=0;}

/*16O Sum Of Gaussians coefficients from H.de Vries et al. ADNDT36(1987)495*/
	RGdendim[8 ][ 0]=2.711; QGdendim[8 ][ 0]= 0.000;
	RGdendim[8 ][ 1]=0.4; QGdendim[8 ][ 1]= 0.057056;
	RGdendim[8 ][ 2]=1.1; QGdendim[8 ][ 2]= 0.195701;
	RGdendim[8 ][ 3]=1.9; QGdendim[8 ][ 3]= 0.311188;
	RGdendim[8 ][ 4]=2.2; QGdendim[8 ][ 4]= 0.224321;
	RGdendim[8 ][ 5]=2.7; QGdendim[8 ][ 5]= 0.059946;
	RGdendim[8 ][ 6]=3.3; QGdendim[8 ][ 6]= 0.135714;
	RGdendim[8 ][ 7]=4.1; QGdendim[8 ][ 7]= 0.000024;
	RGdendim[8 ][ 8]=4.6; QGdendim[8 ][ 8]= 0.013961;
	RGdendim[8 ][ 9]=5.3; QGdendim[8 ][ 9]= 0.000007;
	RGdendim[8 ][10]=5.6; QGdendim[8 ][10]= 0.000002;
	RGdendim[8 ][11]=5.9; QGdendim[8 ][11]= 0.002096;
	RGdendim[8 ][12]=6.4; QGdendim[8 ][12]= 0.000002;
	RGdendim[8 ][18]=1.3; QGdendim[8 ][18]= 0.000000;

/*208Pb Sum Of Gaussians coefficients from H.de Vries et al. ADNDT36(1987)495*/
	RGdendim[82][ 0]=5.503; QGdendim[82][ 0]= 0.000;
	RGdendim[82][ 1]=0.1; QGdendim[82][ 1]= 0.003845;
	RGdendim[82][ 2]=0.7; QGdendim[82][ 2]= 0.009724;
	RGdendim[82][ 3]=1.6; QGdendim[82][ 3]= 0.033093;
	RGdendim[82][ 4]=2.1; QGdendim[82][ 4]= 0.000120;
	RGdendim[82][ 5]=2.7; QGdendim[82][ 5]= 0.083107;
	RGdendim[82][ 6]=3.5; QGdendim[82][ 6]= 0.080869;
	RGdendim[82][ 7]=4.2; QGdendim[82][ 7]= 0.139957;
	RGdendim[82][ 8]=5.1; QGdendim[82][ 8]= 0.260892;
	RGdendim[82][ 9]=6.0; QGdendim[82][ 9]= 0.336013;
	RGdendim[82][10]=6.6; QGdendim[82][10]= 0.033637;
	RGdendim[82][11]=7.6; QGdendim[82][11]= 0.018729;
	RGdendim[82][12]=8.7; QGdendim[82][12]= 0.000020;
	RGdendim[82][18]=1.7; QGdendim[82][18]= 0.000000;
}
