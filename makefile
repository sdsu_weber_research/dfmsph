CCPG    = gcc -O3 -Wall -std=c99 -pg
CC	= gcc -O3 -Wall -std=c99

LIBS = -lm -lncurses
EXECS =  dfmsph

default: $(EXECS)

all: $(EXECS)

dfmsph: dfmsph_mai.c dfmsph_pot.c dfmsph_rho.c dfmsph_exc.c dfmsph_fun.c dfmsph_inp.c dfmsph_out.c dfmsph_def.h
	$(CC) -g dfmsph_mai.c dfmsph_pot.c dfmsph_rho.c dfmsph_exc.c dfmsph_fun.c dfmsph_inp.c dfmsph_out.c  -o dfmsph $(LIBS)

clean :
	rm -f $(EXECS)
	rm -f *.dat
	rm -f *.out
	rm -rf dfmsph.dSYM
	rm -f .DS_Store	
	rm -f number
	rm -f analysis.txt
