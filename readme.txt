The C-code DFMSPH is designed to obtain the nucleus-nucleus potential
by using the double folding model (DFM) and in particular to find the Coulomb
barrier.

In order to perform calculations the user must replace the values of the input
parameters in the file <inp_dfmsph.inp > by those which correspond to the
user�s problem. After that run the file <dfmsph.bat> which compiles, links and
executes the program.

To run the code under Linux OS it is necessary to add the library
#include <ncurses.h> in the file <dfmsph_def.h> and use the following command:
gcc -g dfmsph_mai.c dfmsph_pot.c dfmsph_rho.c dfmsph_exc.c dfmsph_fun.c
dfmsph_inp.c dfmsph_out.c -o dfmsph.exe -lm -lncurses

